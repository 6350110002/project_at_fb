import 'package:flutter/material.dart';
import 'package:project_at_fb/config.dart';

class About extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        drawer: NavigationDrawer(),
        appBar: AppBar(
          centerTitle: true,
          title: Text('About'),
          backgroundColor: Colors.black,
        ),
        body: Container(
            child: SingleChildScrollView(
              child: Column(
                children: [
                  Padding(
                    padding: const EdgeInsets.all(10.0),
                    child: Container(
                      width: 150.0,
                      height: 150.0,
                      alignment: Alignment.center,
                      child: Image.asset("assets/buck.jpg"),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.fromLTRB(20, 10, 1, 20),
                    child: Text(
                      'นายกิตติศักดิ์ สุทธิกิตติพงศ์ 6350110001 ',
                      style: TextStyle(fontSize: 16),
                    ),
                  ),
                  Container(
                    width: 150.0,
                    height: 150.0,
                    alignment: Alignment.center,
                    child: Image.asset("assets/boy.jpg"),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(10.0),
                    child: Text(
                      'นายชญานนท์ หมวดสง 6350110002 ',
                      style: TextStyle(fontSize: 16),
                    ),
                  ),
                  Container(
                    width: 150.0,
                    height: 150.0,
                    alignment: Alignment.center,
                    child: Image.asset("assets/nut.jpg"),
                  ),

                  Padding(
                    padding: const EdgeInsets.all(10.0),
                    child: Text(
                      'นายณัฐภัทร ชัยเพ็ชร 6350110023 ',
                      style: TextStyle(fontSize: 16),
                    ),
                  ),
                  SizedBox(height: 10),
                  Text('   Information and Computer Management',style: TextStyle(fontSize: 20,fontWeight: FontWeight.bold),)
                ],
              ),
            )
        )
    );
  }
}
