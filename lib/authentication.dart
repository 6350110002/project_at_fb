import 'package:firebase_auth/firebase_auth.dart';
import 'package:google_sign_in/google_sign_in.dart';
class AuthenticationHelper {
  final FirebaseAuth _auth = FirebaseAuth.instance;

  get user => _auth.currentUser;

//SIGN UP METHOD
  Future<String?> signUp(
      {required String email, required String password}) async {
    try {
      await _auth.createUserWithEmailAndPassword(
        email: email,
        password: password,
      );
      return null;
    } on FirebaseAuthException catch (e) {
      return e.message;
    }
  }

  //SIGN IN METHOD
  Future<String?> signIn(
      {required String email, required String password}) async {
    try {
      await _auth.signInWithEmailAndPassword(email: email, password: password);
      return null;
    } on FirebaseAuthException catch (e) {
      return e.message;
    }
  }
  Future<void> signOut() async{
    await _auth.signOut();
    bool isGoogleSigned = await GoogleSignIn().isSignedIn();
    if (isGoogleSigned) {
      await GoogleSignIn().signOut();
    }
    print('signout');
  }
  Future<String?> signInWithGoogle() async{
    try{
      final GoogleSignInAccount? googleSignInAccount = await GoogleSignIn().signIn();
      if (googleSignInAccount !=null) {
        final GoogleSignInAuthentication googleSignInAuthentication = await googleSignInAccount
            .authentication;
        final AuthCredential authCredential = GoogleAuthProvider.credential(
            idToken: googleSignInAuthentication.idToken);
        await _auth.signInWithCredential(authCredential);
      }
      return null;
    }  on FirebaseAuthException catch(e){
      return e.message;
    }
  }
}
//
//   //SIGN OUT METHOD
//   Future<void> signOut() async {
//     await _auth.signOut();
//     print('signout');
//   }
// }
