import 'package:fl_chart/fl_chart.dart';
import 'package:flutter/material.dart';
import 'package:project_at_fb/config.dart';


class ChartScreen extends StatefulWidget {
  const ChartScreen({Key? key}) : super(key: key);

  @override
  _ChartScreenState createState() => _ChartScreenState();
}

class _ChartScreenState extends State<ChartScreen> {
  List<Color> gradientColors = [
    const Color(0xff23b6e6),
    const Color(0xff02d39a),
  ];
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: NavigationDrawer(),
      appBar: AppBar(
        centerTitle: true,
        title: Text('Graph Sales Data'),
        backgroundColor: Colors.black,
      ),
      body: Padding(
        padding: const EdgeInsets.fromLTRB(5,60, 40, 30),
        child: Center(
          child: SizedBox(
            width: 400,
            height: 400,
            child: LineChart(
                LineChartData(
                    borderData: FlBorderData(
                        show: true,
                        border: Border.all(color: Colors.black , width: 2)
                    ),
                    gridData: FlGridData(
                      show: true,
                      getDrawingHorizontalLine: (value) {
                        return FlLine(
                            color: Colors.black,
                            strokeWidth: 1
                        );
                      },
                      drawVerticalLine: true,
                      getDrawingVerticalLine: (value) {
                        return FlLine(
                            color: Colors.black,
                            strokeWidth: 1
                        );
                      },
                    ),
                    titlesData: FlTitlesData(
                      show: true,
                      bottomTitles: SideTitles(
                          showTitles: true,
                          reservedSize: 35,
                          getTextStyles: (context, value) {
                            return const TextStyle(
                                color: Color(0xff747d68),
                                fontSize: 16,
                                fontWeight: FontWeight.bold
                            );
                          },
                          getTitles: (value) {
                            switch(value.toInt()){
                              case 0 :
                                return 'Jul 10';
                              case 4 :
                                return 'Aug 10';
                              case 8 :
                                return 'Sep 10';
                              case 12:
                                return 'Oct 10';
                            }
                            return '';
                          },
                          margin: 8
                      ),
                      rightTitles: SideTitles(),
                      topTitles: SideTitles(),
                      leftTitles: SideTitles(
                        showTitles: true,
                        reservedSize: 35,
                        getTextStyles: (context, value) {
                          return const TextStyle(
                              color: Color(0xff68737d),
                              fontSize: 16,
                              fontWeight: FontWeight.bold
                          );
                        },
                        getTitles: (value) {
                          switch(value.toInt()){
                            case 0 :
                              return '0';
                            case 2 :
                              return '25';
                            case 4 :
                              return '50';
                            case 6 :
                              return '100';
                            case 8 :
                              return '200';
                          }
                          return '';
                        },
                        margin: 12,
                      ),
                    ),
                    maxX: 12,
                    maxY: 12,
                    minY: 0,
                    minX: 0,
                    lineBarsData: [
                      LineChartBarData(
                          spots: [
                            const FlSpot(0, 2),
                            const FlSpot(4, 7),
                            const FlSpot(8, 4),
                            const FlSpot(12, 5),
                          ],
                          isCurved: true,
                          colors: [Colors.black],
                          barWidth: 5,
                          belowBarData: BarAreaData(
                              show: true,
                              colors: gradientColors.map((e) => e.withOpacity(0.3)).toList()
                          )
                      )
                    ]
                )
            ),
          ),
        ),
      ),
    );
  }
}