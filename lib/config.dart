import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:project_at_fb/about.dart';
import 'package:project_at_fb/chart_screen.dart';
import 'package:project_at_fb/login.dart';
import 'package:project_at_fb/product.dart';

class fix extends StatefulWidget {
  const fix({Key? key}) : super(key: key);

  @override
  State<fix> createState() => _fixState();
}

class _fixState extends State<fix> {
  @override
  Widget build(BuildContext context) {
    return Scaffold();
  }
}

class NavigationDrawer extends StatelessWidget {
  FirebaseAuth _auth = FirebaseAuth.instance;
  get user => _auth.currentUser;
  @override
  Widget build(BuildContext context) => Drawer(
    child: SingleChildScrollView(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: <Widget>[
          buildHeader(context),
          buildMenuItems(context),
        ],
      ),
    ),
  );
Widget buildHeader(BuildContext context) => Container(
  color: Colors.black,
  child: Padding(
    padding: const EdgeInsets.all(20.0),
    child: Column(
      children: [
        CircleAvatar(
          backgroundColor: Color(0xffE6E6E6),
          radius: 40,
          child: Icon(
            Icons.person,
            color: Color(0xffCCCCCC),
            size: 50,
          ),
        ),
        SizedBox(
          height: 10,
        ),
        Text(user.email,style: TextStyle(fontSize: 18,color: Colors.white),)
      ],
    ),
  ),
  padding: const EdgeInsets.all(24),
);
Widget buildMenuItems(BuildContext context) => Wrap(
  runSpacing: 10,
  children: [
    ListTile(
      leading: const FaIcon(FontAwesomeIcons.house),
      title: const Text('Home'),
      onTap: (){
        Navigator.of(context).push(MaterialPageRoute(builder: (context)=>
            AddProduct()));
      },
    ),
    ListTile(
      leading: const FaIcon(FontAwesomeIcons.chartLine),
      title: const Text('Chart'),
      onTap: (){
        Navigator.of(context).push(MaterialPageRoute(builder: (context)=> ChartScreen()));

      },
    ),
    // ListTile(
    //   leading: const Icon(Icons.add_chart),
    //   title: const Text('Chart'),
    //   onTap: (){
    //     Navigator.of(context).push(MaterialPageRoute(builder: (context)=>
    //         ChartPage()));
    //
    //   },
    // ),
    ListTile(
      leading: const Icon(Icons.people_alt),
      title: const Text('About'),
      onTap: (){
        Navigator.of(context).push(MaterialPageRoute(builder: (context)=>
            About()));
      },
    ),
    ListTile(
      leading: const Icon(Icons.logout),
      title: const Text('Log out'),
      onTap: (){
        Navigator.of(context).push(MaterialPageRoute(builder: (context)=>
            Login()));
      },
    ),
  ],
);
}

